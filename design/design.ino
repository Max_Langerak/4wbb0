// inclusion of our library
#include "Leg.cpp"

// definitions of pins:

// onboard pin
#define LEDPIN   13

// register pins
#define LATCHPIN 8
#define CLOCKPIN 12
#define DATAPIN  11

// camera pins

Leg *leftFrontLeg;
Leg *leftBackLeg;
Leg *rightFrontLeg;
Leg *rightBackLeg;

bool isCentered  = true;
bool hasMoved   = false;
bool hasStarted = false;

void setup() {
    // Set up the first bitch shift register

    pinMode( LATCHPIN, OUTPUT );
    pinMode( DATAPIN, OUTPUT );
    pinMode( CLOCKPIN, OUTPUT );

    leftFrontLeg = new Leg();
    rightFrontLeg = new Leg();
    leftBackLeg = new Leg();
    rightBackLeg = new Leg();

    // set up the serial connections

    // USB connection
    Serial.begin( 9600 );
    // WIFI
    // Serial.begin( 115200 );

    // apparently it is smart to let the machine wake up
    delay( 60000 );

    // move the unit upwards
    rightFrontLeg->moveDown(1, false);
    rightBackLeg->moveDown(1, false);
    leftFrontLeg->moveDown(1, false);
    leftBackLeg->moveDown(1, false);

    hasStarted = true;
}

void loop() {

    if( Serial.available() > 0 && hasStarted ) {
        // we now have serial connection

        char command = Serial.read();

        // we're not using a switch case, as this is more viable
        /* ------------------------------------------------------
         *                   ROBOT  MOVEMENT
         * ------------------------------------------------------ */

        if( command == 'w' ) {

            // new way to move forward (probably faster)
            if( hasMoved ) {

               // moveSet 1

               rightBackLeg->moveUp(1, false);
               leftFrontLeg->moveUp(1, false);

               rightBackLeg->moveForward(1, false);
               leftFrontLeg->moveForward(1, false);

               rightFrontLeg->moveBackward(1, false);
               leftBackLeg->moveBackward(1, true);

               rightBackLeg->moveDown(1, false);
               leftFrontLeg->moveDown(1, true);

               // moveSet 2

               rightFrontLeg->moveUp(1, false);
               leftBackLeg->moveUp(1, false);

               rightFrontLeg->moveForward(1, false);
               leftBackLeg->moveForward(1, false);

               rightBackLeg->moveBackward(1, false);
               leftFrontLeg->moveBackward(1, true);

               rightFrontLeg->moveDown(1, false);
               leftBackLeg->moveDown(1, true);

            } else if(isCentered) {

               // get ready to move
               
               rightFrontLeg->moveUp(1, false);
               leftBackLeg->moveUp(1, false);

               rightFrontLeg->moveForward(1, false);
               leftBackLEg->moveForward(1, true);

               rightFrontLeg->moveDown(1, false);
               leftBackLeg->moveDown(1, true);

               isCentered = false;
               hasMoved  = true;
            } else {
               // center all the legs 
               command = 'c';
            }
        }

        if( command == 'a' ) {
            // rotate left

            leftFrontLeg->moveUp(1, false);
            leftFrontLeg->moveBackward(1, false);
            leftFrontLeg->moveDown(1, false);

            rightBackLeg->moveUp(1, false);
            rightBackLeg->moveForward(1, false);
            rightBackLeg->moveDown(1, false);

            leftBackLeg->moveUp(1, false);
            leftBackLeg->moveBackward(1, false);
            leftBackLeg->moveDown(1, false);

            rightFrontLeg->moveUp(1, false);
            rightFrontLeg->moveForward(1, false);
            rightFrontLeg->moveDown(1, true);

            // move
            leftFrontLeg->moveForward(1, false);
            leftBackLeg->moveForward(1, false);
            rightFrontLeg->moveBackward(1, false);
            rightBackLeg->moveBackward(1, true);
        }

        if( command == 'd' ) {
            // rotate right

            leftFrontLeg->moveUp(1, false);
            leftFrontLeg->moveForward(1, true);
            leftFrontLeg->moveDown(1, false);

            leftBackLeg->moveUp(1, false);
            leftBackLeg->moveForward(1, true);
            leftFrontLeg->moveDown(1, false);

            rightFrontLeg->moveUp(1, false);
            rightFrontLeg->moveBackward(1, true);
            rightFrontLeg->moveDown(1, false);

            rightBackLeg->moveUp(1, false);
            rightBackLeg->moveBackward(1, true);
            rightBackLeg->moveDown(1, true);

            // move
            leftFrontLeg->moveBackward(1, false);
            leftBackLeg->moveBackward(1, false);
            rightFrontLeg->moveForward(1, false);
            rightBackLeg->moveForward(1, true);
        }

        if( command == 's' ) {
            // move backward
        }

        if( command == 'c' ) {
            // center ( reset the robot to ( 0, 0 )
            leftFrontLeg->resetLegX();
            leftFrontLeg->resetLegY();

            leftBackLeg->resetLegX();
            leftBackLeg->resetLegY();

            rightFrontLeg->resetLegX();
            rightFrontLeg->resetLegY();

            rightBackLeg->resetLegX();
            rightBackLeg->resetLegY();

            isCentered = true;
        }

        if( command == 'q' ) {
            // move down
            leftFrontLeg->moveUp(1, false);
            leftBackLeg->moveUp(1, false);
            rightFrontLeg->moveUp(1, false);
            rightBackLeg->moveUp(1, false);
        }

        if( command == 'e' ) {
            // move up
            leftFrontLeg->moveDown(1, false);
            leftBackLeg->moveDown(1, false);
            rightFrontLeg->moveDown(1, false);
            rightBackLeg->moveDown(1, false);
        }
    }
}

void registerWrite( int pin, int state ) {
    byte bitsToSend = 0;

    digitalWrite( LATCHPIN, LOW );

    bitWrite( bitsToSend, pin, state );
    shiftOut( DATAPIN, CLOCKPIN, MSBFIRST, bitsToSend );
    digitalWrite( LATCHPIN, HIGH );
}
