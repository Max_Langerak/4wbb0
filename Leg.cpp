#include "Arduino,h"

class Leg {
    private:
        int m_legYPin_A;
        int m_legYPin_B;

        int m_legXPin_A;
        int m_legXPin_B;

        int m_delayTime = 5;

        int m_deltaY = 0;
        int m_deltaX = 0;

        int m_powerDelta = 0;
        float m_powerRatio = 0;

        bool m_isShifted;

        char m_moveCommandSet[10];
        int m_moveDelaySet[10];

    public:

        Leg();

        void addToMoveSet( char move, int amount );

        void set_Y_pins(int A, int B);
        void set_X_pins(int A, int B);

        int get_pinY_A();
        int get_pinY_B();
        int get_pinX_A();
        int get_pinX_B();

        void moveUp( int amount, bool interval );
        void moveDown( int amount, bool interval );
        void moveForward( int amount, bool interval );
        void moveBackward( int amount, bool interval );

        int getPowerDelta();
        void setPowerDelta( int powerDelta );

        void resetLegX();
        void resetLegY();

        int getDeltaY();
        int getDeltaX();

        void setDeltaX( int delta );
        void setDeltaY( int delta );

        void setPowerRatio( float powerRatio );
        float getPowerRatio();

        bool getShifted();
        void setShifted( bool shifted );

        void move();
};

Leg::Leg() {
   for( int i = 0; i < 10; i++ ) {
      m_moveCommandSet[i] = 'c';
      m_moveDelaySet[i]   = 0;
   }
}

void Leg::addToMoveSet( char command, int amount ) {
   // add a new instruction to the moveset
   for( int i = 0; i < 10; i++ ) {
      if( m_moveCommandSet[i] == 'c' ) {
         m_moveCommandSet[i] = command;
         m_moveDelaySet[i]   = amount;
         break;
      }
   }

}

void Leg::set_Y_pins( int A, int B ) {
    m_legYPin_A = A;
    m_legYPin_B = B;
}

void Leg::set_X_pins( int A, int B ) {
    m_legXPin_A = A;
    m_legXPin_B = B;
}

int Leg::get_pinY_A() { return m_legYPin_A; }
int Leg::get_pinY_B() { return m_legYPin_B; }
int Leg::get_pinX_A() { return m_legXPin_A; }
int Leg::get_pinX_B() { return m_legXPin_B; }

int Leg::getDeltaY() { return m_deltaY; }
int Leg::getDeltaX() { return m_deltaX; }

void Leg::setDeltaX( int delta ) { m_deltaX = delta; }
void Leg::setDeltaY( int delta ) { m_deltaY = delta; }

void Leg::setShifted( bool shifted ) { m_isShifted = shifted; }
bool Leg::getShifted() { return m_isShifted; }

void Leg::setPowerRatio( float powerRatio ) { m_powerRatio = powerRatio; }
float Leg::getPowerRatio() { return m_powerRatio; }

void Leg::setPowerDelta( int powerDelta ) { m_powerDelta = powerDelta; }
int Leg::getPowerDelta() { return m_powerDelta; }

void Leg::moveUp( int amount, bool interval ) {
   this->addToMoveSet( 'u', amount * m_delayTime * m_powerRatio );

   if( interval ) {
      this->move();
   }
}

void Leg::moveDown( int amount, bool interval ) {
   this->addToMoveSet( 'd', amount * m_delayTime * m_powerRatio );

   if( interval ) {
      this->move();
   }
}

void Leg::moveForward( int amount, bool interval ) {
   this->addToMoveSet( 'f', amount * m_delayTime * m_powerRatio );

   if( interval ) {
      this->move(); 
   }
}

void Leg::moveBackward( int amount, bool interval ) {
   this->addToMoveSet( 'b', amount * m_delayTime * m_powerRatio );

   if( interval ) {
      this->move();
   }
}

void Leg::resetLegX() {
    if( this->getDeltaX() < 0 ) {
        this->moveForward( this->getDeltaX() * -1 );
    } else {
        this->moveBackward( this->getDeltaX() );
    }

    this->setDeltaX( 0 );
}

void Leg::resetLegY() {
    if( this->getDeltaY() < 0 ) {
        this->moveUp( this->m_powerRatio * this->getDeltaY() * -1 );
    } else {
        this->moveDown( this->m_powerRatio * this->getDeltaY() );
    }

    this->setDeltaY( 0 );
}

void Leg::move() {
   int heighestIndex    = 0;
   int largestDelayTime = 0;
   int startTime        = millis();
   int deltaTime        = 0;

   // search for the highest index
   for( int i = 0; i < 10; i++ ) {
      if( m_moveCommandSet[i] == 'c' ) {
         heighestIndex = i;
         break;
      }
   }

   // find the largest delayTime 
   for( int i = 0; i < heighestIndex; i++ ) {
      if( m_moveDelaySet[i] > largestDelayTime ) {
         largestDelayTime = m_moveDelaySet[i];
      }
   }

   // get the instructions and call it
   for( int i = 0; i < heighestIndex; i++ ) {

      switch( m_moveCommandSet[i] ) {
         case 'u':
            digitalWrite( this->m_legYPin_A, HIGH );
            digitalWrite( this->m_legYPin_B, LOW  );
            break;
         case 'd':
            digitalWrite( this->m_legYPin_A, LOW );
            digitalWrite( this->m_legYPin_B, HIGH );
            break;
         case 'f':
            digitalWrite( this->m_legXPin_A, HIGH );
            digitalWrite( this->m_legXPin_B, LOW );
            break;
         case 'b':
            digitalWrite( this->m_legXPin_A, LOW );
            digitalWrite( this->m_legXPin_B, HIGH );
            break;
      }
   }
   
   while( deltaTime - 20 < largestDelayTime ) {
      
      deltaTime = millis() - startTime;

      for( var i = 0; i < heighestIndex; i++ ) {
         if( deltaTime > m_moveDelaySet[i] ) {
            // turn off the motor that is here 
            switch( m_moveCommandSet[i] ) {
               case 'u':
                  digitalWrite( this->m_legYPin_A, LOW );
                  break;
               case 'd':
                  digitalWrite( this->m_legYPin_B, LOW );
                  break;
               case 'f':
                  digitalWrite( this->m_legXPin_A, LOW );
                  break;
               case 'b':
                  digitalWrite( this->m_legXPin_B, LOW );
                  break;

            }
         }
      }
   }

   // reset everything
   for( int i = 0; i < heighestIndex; i++ ) {
      this->m_moveCommandSet[i] = 'c';
      this->m_moveDelaySet[i] = 0;
   }
}
